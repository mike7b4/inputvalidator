use lazy_static::lazy_static;
use std::fmt;
use std::net::Ipv4Addr;
use std::str::FromStr;

use regex::Regex;
use serde::{de, Deserialize, Deserializer, Serialize};

#[derive(Debug, Serialize, Deserialize)]
enum IpOrHost {
    #[serde(deserialize_with = "deserialize_ip_or_host")]
    Ip(Ipv4Addr),
    #[serde(deserialize_with = "deserialize_ip_or_host")]
    Host(String),
}

#[derive(Deserialize, Serialize)]
struct Netmask {
    #[serde(deserialize_with = "deserialize_netmask")]
    netmask: Ipv4Addr,
}

fn deserialize_netmask<'de, S, D>(deserializer: D) -> Result<S, D::Error>
where
    S: FromStr,
    S::Err: fmt::Display,
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    let ip = S::from_str(&s).map_err(de::Error::custom)?;
    let regex = Regex::new(r"^(255)\.(0|128|192|224|240|248|252|254|255)\.(0|128|192|224|240|248|252|254|255)\.(0|128|192|224|240|248|252|254|255)").unwrap();
    if !regex.is_match(&s) {
        return Err(de::Error::custom("Not a valid netmask".to_string()));
    }
    Ok(ip)
}

fn deserialize_ip_or_host<'de, S, D>(deserializer: D) -> Result<S, D::Error>
where
    S: FromStr,
    S::Err: fmt::Display,
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    Ok(S::from_str(&s).map_err(de::Error::custom))?
}

#[derive(Deserialize, Serialize)]
struct Dhcp4 {
    ip: Ipv4Addr,
    start: Ipv4Addr,
    end: Ipv4Addr,
    #[serde(flatten)]
    inner_mask: Netmask,
}

impl Dhcp4 {
    fn new(json: &str) -> Result<Self, Error> {
        serde_json::from_str(&json).map_err(|error| {
            Error::Message(
                error
                    .to_string()
                    .split(" at line")
                    .collect::<Vec<&str>>()
                    .get(0)
                    .unwrap()
                    .to_string(),
            )
        })
    }
}

impl fmt::Display for IpOrHost {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Ip(ip) => write!(f, "{ip}"),
            Self::Host(host) => write!(f, "{host}"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
enum Error {
    Message(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Message(cause) => write!(f, "{cause}"),
        }
    }
}

impl FromStr for IpOrHost {
    type Err = Error;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if let Ok(ip) = Ipv4Addr::from_str(input) {
            return Ok(Self::Ip(ip));
        }
        let input = input.trim();
        // back to regex
        let regex =
            Regex::new(r"^([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*)+(\.([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*))*$")
                .unwrap();

        if regex.is_match(input) {
            return Ok(Self::Host(input.into()));
        }

        Err(Error::Message("Expects an IP or Host".to_string()))
    }
}

lazy_static! {
    pub static ref RE_EMAIL: Regex = Regex::new(r#"^[\w\-\.]+@([\w-]+\.)+[\w-]{1,}$"#).unwrap();
}

struct Email;
impl FromStr for Email {
    type Err = Error;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if RE_EMAIL.is_match("email@email.com") {
            return Ok(Self);
        }
        Err(Error::Message(format!("{input:?} is not an email")))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_email() {
        Email::from_str("lala@lala.com").expect("Valid email");
        Email::from_str("lala_lala@lala.com").expect("Valid email");
        Email::from_str("lalalala@la_la.com").expect("Valid email");
        Email::from_str("lalalala@la-la.com").expect("Valid email");
        Email::from_str("11111111@lala.com").expect("Valid email");
        Email::from_str("11111-111@lala.com").expect("Valid email");

        Email::from_str("11111 111@lala.com").expect("Invalid email");
        Email::from_str("♥@cpp.com").expect("Invalid email");
        Email::from_str("11111111@l la.com").expect("Invalid email");
        Email::from_str("@lala.com").expect("Invalid email");
        Email::from_str("lala@.com").expect("Invalid email");
        Email::from_str("lala@com").expect("Invalid email");
    }
}

fn main() {
    println!(
        "{}",
        serde_json::to_string_pretty(&IpOrHost::from_str("10.10.10.10").unwrap()).unwrap()
    );
    println!(
        "{}",
        serde_json::to_string_pretty(&IpOrHost::from_str("host.lala").unwrap()).unwrap()
    );
    println!(
        "{}",
        serde_json::to_string_pretty(
            &IpOrHost::from_str("host no go lala").map_err(|e| Error::Message(format!("{e}")))
        )
        .unwrap()
    );
    println!(
        "{:?}",
        serde_json::from_str::<IpOrHost>(r#"{"Ip":"1.1.1.1"}"#).unwrap()
    );

    println!("{}", serde_json::to_string_pretty(&Dhcp4::new(r#"{"ip" : "1.1.1.1", "start" : "2.3.4.5", "end" : "2.2.2.2", "netmask" : "255.255.255.0"}"#)).unwrap());
}
